﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour

{
	
	public float maxspeed = 6;
	public float rotspeed = 2;
	public bool isEnabled = true;

	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update ()
	{

		if (Input.GetKeyUp (KeyCode.P)) {
			isEnabled = !isEnabled; 
		}
		if (Input.GetKeyUp (KeyCode.Q)) {
			gameObject.SetActive (false);
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			transform.position = new Vector3 (0, 0, 0);
		}
		if (isEnabled) {
			if (Input.GetKey (KeyCode.LeftShift) || (Input.GetKey (KeyCode.RightShift)) == true) {
			
				// WASD Single Unit Controls
				//forward movement with w
				if (Input.GetKeyDown (KeyCode.W)) {
					transform.Translate (Vector2.up);
				}
				//backward movement with s
				if (Input.GetKeyDown (KeyCode.S)) {
					transform.Translate (Vector2.down);
				}
				//left rotation with a
				if (Input.GetKeyDown (KeyCode.A)) {
					transform.Translate (Vector2.left);
				}
				//right rotation with d
				if (Input.GetKeyDown (KeyCode.D)) {
					transform.Translate (Vector2.right);
				}

				// Arrow Key Single Unit Controls

				//forward with up arrow
				if (Input.GetKeyDown (KeyCode.UpArrow)) {
					transform.Translate (Vector2.up);
				}
				//backward with down arrow
				if (Input.GetKeyDown (KeyCode.DownArrow)) {
					transform.Translate (Vector2.down);
				}
				//left rotation with left arrow
				if (Input.GetKeyDown (KeyCode.LeftArrow)) {
					transform.Translate (Vector2.left);
				}
				//right rotation with right arrow
				if (Input.GetKeyDown (KeyCode.RightArrow)) {
					transform.Translate (Vector2.right);
				}
			} 
			else {
				// WASD Controls

				//forward movement with w
				if (Input.GetKey (KeyCode.W)) {
					transform.Translate (Vector2.up * maxspeed * Time.deltaTime);
				}
				//backward movement with s
				if (Input.GetKey (KeyCode.S)) {
					transform.Translate (Vector2.down * maxspeed * Time.deltaTime);
				}
				//left rotation with a
				if (Input.GetKey (KeyCode.A)) {
					transform.Translate (Vector2.left* rotspeed);
				}
				//right rotation with d
				if (Input.GetKey (KeyCode.D)) {
					transform.Translate (Vector2.right * rotspeed);
				}

				// Arrow Key Controls

				//forward with up arrow
				if (Input.GetKey (KeyCode.UpArrow)) {
					transform.Translate (Vector2.up * maxspeed * Time.deltaTime);
				}
				//backward with down arrow
				if (Input.GetKey (KeyCode.DownArrow)) {
					transform.Translate (Vector2.down * maxspeed * Time.deltaTime);
				}
				//left rotation with left arrow
				if (Input.GetKey (KeyCode.LeftArrow)) {
					transform.Translate (Vector2.left * rotspeed);
				}
				//right rotation with right arrow
				if (Input.GetKey (KeyCode.RightArrow)) {
					transform.Translate (Vector2.right * rotspeed);
				}
			}
		}
//			the following is the code for a boost functionality. May implement later.


//			if (Input.GetKey (KeyCode.LeftShift) == true)
//			{
//				if (Input.GetKey (KeyCode.W))
//				{
//					transform.Translate (Vector2.up * maxspeed * Time.deltaTime * 3);
//				}
//				if (Input.GetKey (KeyCode.UpArrow))
//				{
//					transform.Translate (Vector2.up * maxspeed * Time.deltaTime * 3);
//				}
//			}
	}
}
