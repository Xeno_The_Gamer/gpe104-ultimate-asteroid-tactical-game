﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

	public Player pmComp;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.P)) {
			pmComp.isEnabled = !pmComp.isEnabled; 
		}
		if (Input.GetKeyUp (KeyCode.Q)) {
			pmComp.gameObject.SetActive (false);
		}
	}
}
